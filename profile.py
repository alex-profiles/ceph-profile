"""Ceph profile

Instructions:
This will automatically bring up ceph with 3 monitors and OSDs as specified.
To test this out, log in to the ceph_test node {host-ceph_test} and run:

```
/local/repository/mount_ceph.sh
```
"""

import geni.portal as portal
import geni.rspec.pg as rspec

node_types = {
    "pc3000": (1,
               "Emulab pc3000 nodes 2x1-core 2GB RAM 1-4x1Gb, 1-4x10/100Mb"),
    "d710": (1, "Emulab d710 nodes 1x4-core 12GB RAM 4x1Gb"),
    "d430": (10, "Emulab d430 nodes 2x8-core 64GB RAM 2/4x10Gb 4/2x1Gb"),
    "d740": (10, "POWDER d740 nodes 2x16-core 96 GB RAM 2x10Gb"),
    "d840": (40, "POWDER d840 nodes 2x16-core 96 GB RAM 2x40Gb"),
    "d820": (10, "Emulab d820 nodes 4x8-core 128 GB RAM 4x10Gb"),
    "m400": (10, "CloudLab m400 nodes 1x8 ARMv8 64 GB RAM 2x10Gb"),
    "c240g1": (10,
               "CloudLab Wisconsin c240g1 nodes 2x8-core 128GB RAM "
               "12x3TB HDD, 2x480GB SSD 2x10Gb"),
    "c220g1": (10,
               "CloudLab Wisconsin c220g1 nodes 2x8-core 128GB RAM "
               "2x1.2TB HDD, 1x480GB SSD 2x10Gb"),
    "c220g2": (10,
               "CloudLab Wisconsin c220g2 nodes 2x10-core 160GB RAM "
               "2x1.2TB HDD, 1x480GB SSD 2x10Gb"),
    "c220g5": (10,
               "CloudLab Wisconsin c220g5 nodes 2x10-core 192GB RAM "
               "1x1TB HDD, 1x480GB SSD 2x10Gb"),
}

node_type_choices = tuple(
    [(key, value[1]) for key, value in node_types.items()]
)


pc = portal.Context()

pc.defineParameter("mon_hw_type", "Hardware Type for Monitors",
                   portal.ParameterType.STRING, "d430",
                   node_type_choices,
                   longDescription="Node resource type"
                  )


pc.defineParameter("osd_hw_type", "Hardware Type for OSDs",
                   portal.ParameterType.STRING, "d740",
                   node_type_choices,
                   longDescription="Node resource type"
                  )


pc.defineParameter("num_osd_nodes", "Number of OSD nodes",
                   portal.ParameterType.INTEGER, 3,
                   longDescription="Node resource type"
                  )

pc.defineParameter("osd_drives",
                   "What devices to use for OSDs (comma separated)",
                   portal.ParameterType.STRING, "sdb",
                   longDescription=
                   "Comma separated list of device fils to use for OSDs"
                  )


pc.defineParameter("test_hw_type", "Hardware Type for Test node",
                   portal.ParameterType.STRING, "d740",
                   node_type_choices,
                   longDescription="Node resource type"
                  )


params = pc.bindParameters()
pc.verifyParameters()

request = portal.context.makeRequestRSpec()

nodes_ceph_mon = []
nodes_ceph_osd = []


link_fe = request.LAN("lan_ceph_fe")
link_be = request.LAN("lan_ceph_be")

link_fe_idx = 1
link_be_idx = 1


# mon
for i in range(3):
    node_ceph_mon = request.RawPC("ceph_mon{0}".format(i))
    if i == 0:
        command_template = "/local/repository/startup-ceph-mon.sh {0}"
    else:
        command_template = "/local/repository/startup-ceph-mon-others.sh {0}"
    command = command_template.format(params.osd_drives)
    node_ceph_mon.addService(rspec.Execute(shell="bash", command=command))
    node = node_ceph_mon

    # Need this because ceph provisioner has problems with old kernel https://github.com/kubernetes-incubator/external-storage/issues/345
    #node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    # Need >= 2020 to get rtslib-fb to a new enough version for ceph-iscsi
    node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD"


    # External network for ceph
    iface_fe = node.addInterface("if3")
    iface_fe.addAddress(rspec.IPv4Address("192.168.3.%d"%(link_fe_idx),
                                         "255.255.255.0"))
    link_fe_idx += 1

    #if node.hardware_type in xg_exp_types:
    #    iface_fe.bandwidth = 10*1000*1000
    #else:
    #    iface_fe.bandwidth = 1000*1000
    iface_fe.bandwidth = 1000*1000

    if node.hardware_type == "m400":
        iface_fe.link_multiplexing = True
        iface_fe.vlan_tagging = True

    link_fe.addInterface(iface_fe)

    node_ceph_mon.hardware_type = params.mon_hw_type
    nodes_ceph_mon.append(node_ceph_mon)

# osd
for i in range(params.num_osd_nodes):
    node_ceph_osd = request.RawPC("ceph_osd{0}".format(i))
    command_template = "/local/repository/startup-ceph-osd.sh {0}"
    command = command_template.format(params.osd_drives)
    node_ceph_osd.addService(
        rspec.Execute(shell="bash",
                      command=command))
    node_ceph_osd.hardware_type = params.osd_hw_type
    nodes_ceph_osd.append(node_ceph_osd)
    node = node_ceph_osd

    # Need this because ceph provisioner has problems with old kernel https://github.com/kubernetes-incubator/external-storage/issues/345
    #node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    # Need >= 2020 to get rtslib-fb to a new enough version for ceph-iscsi
    node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD"

    # External network for ceph
    iface_fe = node.addInterface("if3")
    iface_fe.addAddress(rspec.IPv4Address("192.168.3.%d"%(link_fe_idx),
                                         "255.255.255.0"))
    link_fe_idx += 1

    iface_fe.bandwidth = node_types[node.hardware_type][0]*1000*1000

    if node.hardware_type == "m400":
        iface_fe.link_multiplexing = True
        iface_fe.vlan_tagging = True

    # Internal network for ceph
    iface_be = node.addInterface("if4")
    iface_be.addAddress(rspec.IPv4Address("192.168.4.%d"%(link_be_idx),
                                         "255.255.255.0"))
    link_be_idx += 1

    iface_be.bandwidth = node_types[node.hardware_type][0]*1000*1000

    if node.hardware_type == "m400":
        iface_be.link_multiplexing = True
        iface_be.vlan_tagging = True

    link_fe.addInterface(iface_fe)
    link_be.addInterface(iface_be)


node_ceph_test = request.RawPC("ceph_test")
node_ceph_test.hardware_type = params.test_hw_type
node_ceph_test.addService(
    rspec.Execute(shell="bash",
                  command="/local/repository/ceph-common.sh"))
node = node_ceph_test


# Need this because ceph provisioner has problems with old kernel https://github.com/kubernetes-incubator/external-storage/issues/345
#node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
# Need >= 2020 to get rtslib-fb to a new enough version for ceph-iscsi
node.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD"

# External network for ceph
iface_fe = node.addInterface("if3")
iface_fe.addAddress(rspec.IPv4Address("192.168.3.%d"%(link_fe_idx),
                                     "255.255.255.0"))
link_fe_idx += 1

iface_fe.bandwidth = node_types[node.hardware_type][0]*1000*1000

if node.hardware_type == "m400":
    iface_fe.link_multiplexing = True
    iface_fe.vlan_tagging = True

link_fe.addInterface(iface_fe)


all_nodes = nodes_ceph_mon + nodes_ceph_osd + [node_ceph_test]


portal.context.printRequestRSpec()
