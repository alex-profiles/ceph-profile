#!/bin/bash

EXP_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $2}'`
PROJ_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`

sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.client.admin.secret /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.client.admin.keyring /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.mon.keyring /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.initial-monmap /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.conf /etc/ceph/

sudo cp /etc/ceph/ceph.client.admin.secret /root/
sudo sed "s/^[\t ]*key = //g" -i /root/ceph.client.admin.secret

sudo mkdir /mnt/ceph
sudo mount -t ceph -o name=admin,secretfile=/root/ceph.client.admin.secret ceph_mon0-lan_ceph_fe:/ /mnt/ceph
