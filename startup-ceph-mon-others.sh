#!/bin/bash

/local/repository/ceph-common.sh

EXP_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $2}'`
PROJ_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`
NUM_OSDS=`geni-get manifest | grep num_osds | sed "s/.*>\(.*\)<.*/\1/g"`

# Guess I don't need this bit?
#MON0=`grep ceph_mon0-lan_ceph_fe /etc/hosts | cut -f 1`
#MON1=`grep ceph_mon1-lan_ceph_fe /etc/hosts | cut -f 1`
#MON2=`grep ceph_mon2-lan_ceph_fe /etc/hosts | cut -f 1`

#OSD0_FE=`grep ceph_osd0-lan_ceph_fe /etc/hosts | cut -f 1`
#OSD1_FE=`grep ceph_osd1-lan_ceph_fe /etc/hosts | cut -f 1`
#OSD2_FE=`grep ceph_osd2-lan_ceph_fe /etc/hosts | cut -f 1`
#OSD0_BE=`grep ceph_osd0-lan_ceph_be /etc/hosts | cut -f 1`
#OSD1_BE=`grep ceph_osd1-lan_ceph_be /etc/hosts | cut -f 1`
#OSD2_BE=`grep ceph_osd2-lan_ceph_be /etc/hosts | cut -f 1`

nc -l -p 1234

sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.client.admin.secret /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.client.admin.keyring /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.mon.keyring /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.initial-monmap /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.conf /etc/ceph/

MON_IDX=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $1}' | sed "s/ceph_mon//"`

sudo ceph-mon --mkfs -i ${MON_IDX} --monmap /etc/ceph/ceph.initial-monmap --keyring /etc/ceph/ceph.mon.keyring
sudo chown -R ceph:ceph /var/lib/ceph/mon

sudo systemctl enable ceph-mon@${MON_IDX}
sudo systemctl start ceph-mon@${MON_IDX}

