#!/bin/bash -ex

/local/repository/ceph-common.sh

#MON1=`grep ceph_mon1-lan_ceph_fe /etc/hosts | cut -f 1`
#MON2=`grep ceph_mon2-lan_ceph_fe /etc/hosts | cut -f 1`
#MON3=`grep ceph_mon3-lan_ceph_fe /etc/hosts | cut -f 1`

#OSD1_FE=`grep ceph_osd1-lan_ceph_fe /etc/hosts | cut -f 1`
#OSD2_FE=`grep ceph_osd2-lan_ceph_fe /etc/hosts | cut -f 1`
#OSD3_FE=`grep ceph_osd3-lan_ceph_fe /etc/hosts | cut -f 1`
#OSD1_BE=`grep ceph_osd1-lan_ceph_be /etc/hosts | cut -f 1`
#OSD2_BE=`grep ceph_osd2-lan_ceph_be /etc/hosts | cut -f 1`
#OSD3_BE=`grep ceph_osd3-lan_ceph_be /etc/hosts | cut -f 1`

EXP_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $2}'`
PROJ_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`

nc -l -p 1234

sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.client.admin.secret /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.client.admin.keyring /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.mon.keyring /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.initial-monmap /etc/ceph/
sudo cp -p /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/ceph.conf /etc/ceph/



sudo sh -c "ceph auth get client.bootstrap-osd > /var/lib/ceph/bootstrap-osd/ceph.keyring"



OSD_NODE_IDX=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $1}' | sed "s/ceph_osd//"`

IFS="," read -r -a OSDS <<< $1
echo "Num OSDs: ${#OSDS[@]}"

OSDS_PER_NODE=${#OSDS[@]}
FIRST_OSD_IDX=$((${OSDS_PER_NODE}*${OSD_NODE_IDX}))

volumes=""
sizes=""

for i in $(seq 0 $((${OSDS_PER_NODE}-1)))
do
    OSD_IDX=$((${FIRST_OSD_IDX}+$i))
    echo ${OSDS[$i]} ${OSD_IDX}
    volumes+="ceph-osd${OSD_IDX}-db,ceph-osd${OSD_IDX}-wal,"
    sizes+="10G,10G,"
done

volumes=`echo "${volumes%,}"`
sizes=`echo "${sizes%,}"`

sudo /usr/local/etc/emulab/mkextrafs.pl \
    -v vg_ceph \
    -m ${volumes} \
    -z ${sizes} \
    -l

for i in $(seq 0 $((${OSDS_PER_NODE}-1)))
do
    osd_uuid=`uuidgen`
    OSD_IDX=$((${FIRST_OSD_IDX}+$i))
    BLOCK_DATA=/dev/${OSDS[$i]}
    BLOCK_DB=/dev/vg_ceph/ceph-osd${OSD_IDX}-db
    BLOCK_WAL=/dev/vg_ceph/ceph-osd${OSD_IDX}-wal

    #sudo ceph-volume raw prepare --bluestore \
    #    --data /dev/sdb \
    #    --block.db /dev/vg_ceph/ceph-osd${OSD_IDX}-db \
    #    --block.wal /dev/vg_ceph/ceph-osd${OSD_IDX}-wal
    # Doing what the above does, just manually instead
    sudo /usr/bin/ceph-authtool --gen-print-key
    osd_id=`sudo /usr/bin/ceph --cluster ceph --name client.bootstrap-osd \
        --keyring /var/lib/ceph/bootstrap-osd/ceph.keyring \
        osd new ${osd_uuid} ${OSD_IDX}`
    sudo /usr/bin/ceph-authtool --gen-print-key
    sudo mkdir -p /var/lib/ceph/osd/ceph-${OSD_IDX}
    sudo /usr/bin/ln -s ${BLOCK_DATA} /var/lib/ceph/osd/ceph-${OSD_IDX}/block
    sudo /usr/bin/ceph --cluster ceph --name client.bootstrap-osd \
        --keyring /var/lib/ceph/bootstrap-osd/ceph.keyring mon getmap \
        -o /var/lib/ceph/osd/ceph-${OSD_IDX}/activate.monmap
    sudo sh -c "ceph auth get-or-create osd.${OSD_IDX} \
        osd 'allow *' mon 'allow profile osd' \
        > /var/lib/ceph/osd/ceph-${OSD_IDX}/keyring"
    sudo ln -s ${BLOCK_DB} /var/lib/ceph/osd/ceph-${OSD_IDX}/block.db
    sudo ln -s ${BLOCK_WAL} /var/lib/ceph/osd/ceph-${OSD_IDX}/block.wal
    sudo chown ceph:ceph ${BLOCK_DATA} ${BLOCK_DB} ${BLOCK_WAL}
    sudo chown ceph:ceph -R /var/lib/ceph/osd/ceph-${OSD_IDX}
    #sudo /usr/bin/ceph-osd --cluster ceph --osd-objectstore bluestore \
    #    --mkfs -i ${OSD_IDX} \
    #    --monmap /var/lib/ceph/osd/ceph-${OSD_IDX}/activate.monmap \
    #    --bluestore-block-wal-path /dev/vg_ceph/ceph-osd${OSD_IDX}-wal \
    #    --bluestore-block-db-path /dev/vg_ceph/ceph-osd${OSD_IDX}-db \
    #    --osd-data /var/lib/ceph/osd/ceph-${OSD_IDX}/block \
    #    --osd-uuid ${osd_uuid} \
    #    --setuser ceph --setgroup ceph
    sudo ceph-osd -i ${osd_id} --mkfs --osd-uuid ${osd_uuid}

    sudo ceph osd crush add-bucket ceph-osd-${OSD_IDX} host
    sudo ceph osd crush move ceph-osd-${OSD_IDX} root=default
    sudo ceph osd crush add osd.${OSD_IDX} 1.0 host=ceph-osd-${OSD_NODE_IDX}

    sudo chown ceph:ceph -R /var/lib/ceph/osd/ceph-${OSD_IDX}

    #sudo ceph-volume raw activate ${OSD_IDX} ${osd_uuid}


    #sudo /usr/local/etc/emulab/mkextrafs.pl -v vg_system -m ceph -l
    #osd_id=`sudo ceph osd create ${osd_uuid} ${OSD_IDX}`
    #sudo -u ceph mkdir -p /var/lib/ceph/osd/ceph-${OSD_IDX}

    #sudo ceph-authtool --create-keyring /var/lib/ceph/osd/ceph-${OSD_IDX}/keyring --name osd.${OSD_IDX} --add-key `ceph-authtool --gen-print-key`
    #sudo ceph auth add osd.${OSD_IDX} osd 'allow *' mon 'allow profile osd' -i /var/lib/ceph/osd/ceph-${OSD_IDX}/keyring
    #sudo ceph-osd -i ${osd_id} --mkfs --osd-uuid ${osd_uuid}
    #sudo ceph osd crush add-bucket ceph-osd-${OSD_IDX} host
    #sudo ceph osd crush move ceph-osd-${OSD_IDX} root=default
    #sudo ceph osd crush add osd.${OSD_IDX} 1.0 host=ceph-osd-${OSD_IDX}

    sudo systemctl enable ceph-osd@${OSD_IDX}
    sudo systemctl start ceph-osd@${OSD_IDX}
done
