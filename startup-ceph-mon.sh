#!/bin/bash -ex

/local/repository/ceph-common.sh

NUM_OSD_NODES=`geni-get manifest | grep num_osd_nodes | sed "s/.*>\(.*\)<.*/\1/g"`

sudo ceph-authtool --create-keyring /etc/ceph/ceph.client.admin.keyring --gen-key -n client.admin --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow'
sudo sh -c "umask 177; grep \"key = \" /etc/ceph/ceph.client.admin.keyring | awk '{print $3}' > /etc/ceph/ceph.client.admin.secret"
sudo ceph-authtool --create-keyring /etc/ceph/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *' --import-keyring /etc/ceph/ceph.client.admin.keyring

MON0=`grep ceph_mon0-lan_ceph_fe /etc/hosts | cut -f 1`
MON1=`grep ceph_mon1-lan_ceph_fe /etc/hosts | cut -f 1`
MON2=`grep ceph_mon2-lan_ceph_fe /etc/hosts | cut -f 1`
sudo monmaptool --create --fsid `uuidgen` \
    --add 0 $MON0 --add 1 $MON1 --add 2 $MON2 \
    /etc/ceph/ceph.initial-monmap

FSID_UUID=`uuidgen`

#OSD2_FE=`grep ceph_osd2-lan_ceph_fe /etc/hosts | cut -f 1`
#OSD3_FE=`grep ceph_osd3-lan_ceph_fe /etc/hosts | cut -f 1`
#OSD2_BE=`grep ceph_osd2-lan_ceph_be /etc/hosts | cut -f 1`
#OSD3_BE=`grep ceph_osd3-lan_ceph_be /etc/hosts | cut -f 1`

mkdir /local/geniuser
cd /local/geniuser

cat > ceph.conf <<END
[global]
  fsid = $FSID_UUID
  cluster = ceph
  public network = 192.168.3.0/24
  cluster network = 192.168.4.0/24

  auth cluster required = cephx
  auth service required = cephx
  auth client required = cephx

  osd pool default size = 3
  osd pool default min size = 1

[mon]
  mon host = ceph-mon-0 ceph-mon-1 ceph-mon-2
  mon addr = $MON0 $MON1 $MON2
  mon initial members = 0, 1, 2

[mon.0]
  host = ceph-mon-0
  mon addr = $MON0:6789

[mon.1]
  host = ceph-mon-1
  mon addr = $MON1:6789

[mon.2]
  host = ceph-mon-2
  mon addr = $MON2:6789

[osd]
  osd journal size = 1024
  filestore xattr use omap = true
  osd mkfs type = ext4
  osd mount options ext4 = user_xattr,rw,noatime
  osd max object name len = 256
  osd max object namespace len = 64

END

IFS="," read -r -a OSDS <<< $1
echo "Num OSDs: ${#OSDS[@]}"

OSDS_PER_NODE=${#OSDS[@]}

for i in $(seq 0 $((${NUM_OSD_NODES}-1)))
do
    FIRST_OSD_IDX=$((${OSDS_PER_NODE}*${i}))

    for j in $(seq 0 $((${OSDS_PER_NODE}-1)))
    do
        OSD_IDX=$((${FIRST_OSD_IDX}+$j))
        BLOCK_DATA=/dev/${OSDS[$j]}

        OSD_FE=`grep ceph_osd$i-lan_ceph_fe /etc/hosts | cut -f 1`
        OSD_BE=`grep ceph_osd$i-lan_ceph_be /etc/hosts | cut -f 1`

cat >> ceph.conf <<END
[osd.${OSD_IDX}]
  host = ceph-osd-$i
  devs = ${BLOCK_DATA}
  public addr = ${OSD_FE}
  cluster addr = ${OSD_BE}

END
    done
done

cat >> ceph.conf <<END
[mds.m0]
  host = ceph-mon-0

[mds.m1]
  host = ceph-mon-1

[mds.m2]
  host = ceph-mon-2
END

sudo cp ceph.conf /etc/ceph/ceph.conf
rm ceph.conf

sudo ceph-mon --mkfs -i 0 --monmap /etc/ceph/ceph.initial-monmap --keyring /etc/ceph/ceph.mon.keyring
sudo chown -R ceph:ceph /var/lib/ceph/mon

sudo systemctl enable ceph-mon@0
sudo systemctl start ceph-mon@0

EXP_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $2}'`
PROJ_NAME=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`

sudo cp -p /etc/ceph/ceph.client.admin.secret /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/
sudo cp -p /etc/ceph/ceph.client.admin.keyring /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/
sudo cp -p /etc/ceph/ceph.mon.keyring /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/
sudo cp -p /etc/ceph/ceph.initial-monmap /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/
sudo cp -p /etc/ceph/ceph.conf /proj/$PROJ_NAME/exp/$EXP_NAME/tmp/


echo Poking ceph_mon1
while ! nc -z ceph_mon1-lan_ceph_fe 1234; do
    sleep 1
done
echo Poking ceph_mon2
while ! nc -z ceph_mon2-lan_ceph_fe 1234; do
    sleep 1
done
for i in $(seq 0 $((${NUM_OSD_NODES}-1)))
do
    echo Poking ceph_osd$i
    while ! nc -z ceph_osd$i-lan_ceph_fe 1234; do
        sleep 1
    done
done

sudo mkdir -p /var/lib/ceph/mgr/ceph-0
sudo sh -c "ceph auth get-or-create mgr.0 \
    mon 'allow profile mgr' osd 'allow *' mds 'allow *' \
    > /var/lib/ceph/mgr/ceph-0/keyring"
sudo ceph-mgr -i 0
sudo ceph auth caps client.admin \
    mds 'allow' mon 'allow *' osd 'allow *' mgr 'allow *'

#sudo -u ceph mkdir -p /var/lib/ceph/mgr/ceph-0
#sudo ceph auth get-or-create mgr.0 mon 'allow profile mgr' osd 'allow *' mds 'allow *' > /var/lib/ceph/mgr/ceph-0/keyring

sudo -u ceph mkdir -p /var/lib/ceph/mds/ceph-m0
sudo sh -c "ceph auth get-or-create mds.m0 mds 'allow' osd 'allow *' mon 'allow rwx' > /var/lib/ceph/mds/ceph-m0/keyring"
sudo ceph-mds --cluster ceph -i m0 -m ceph_mon0-lan_ceph_fe:6789

# Don't do this as I think the above line already starts it up?
# If you do, there will be 2 daemons running that will thrash against each
# other
#sudo systemctl enable ceph-mds@m0
#sudo systemctl start ceph-mds@m0

sleep 1

sudo ceph osd pool create cephfs_data 64
sudo ceph osd pool create cephfs_metadata 64

sudo ceph fs new root_ceph cephfs_metadata cephfs_data
